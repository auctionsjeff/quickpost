﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QuickPost
{
    public class QuickPostConfig
    {
        [XmlElement]
        public string WebsiteUrl;
        [XmlElement]
        public string Username;
        [XmlElement]
        public string Password;
        [XmlElement]
        public string Category;
        [XmlElement]
        public string DefaultPostTitle;
        [XmlElement]
        public string DefaultPostBody;
        [XmlElement]
        public string AudioFileType;
        [XmlElement]
        public decimal AudioVolumeModifier;
        [XmlElement]
        public int AudioSampleRateHz;
        [XmlElement]
        public string CustomSoxArgs;
        [XmlElement]
        public string SoxPath;
        [XmlElement]
        public int AudioChannelCount;

    }
}
