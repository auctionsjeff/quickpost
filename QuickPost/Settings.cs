﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuickPost
{
    public partial class Settings : Form
    {
        public Settings(QuickPostConfig _config)
        {
            InitializeComponent();

            txtUrl.Text = _config.WebsiteUrl;
            txtUser.Text = _config.Username;
            txtPass.Text = _config.Password;
            txtCategory.Text = _config.Category;

            txtDefaultTitle.Text = _config.DefaultPostTitle;
            txtDefaultBody.Text = _config.DefaultPostBody;

            txtChannels.Text = _config.AudioChannelCount.ToString();
            txtSampleRate.Text = _config.AudioSampleRateHz.ToString();
            txtCustomArgs.Text = _config.CustomSoxArgs;
            txtVolumeModifier.Text = _config.AudioVolumeModifier.ToString();
            txtFileType.Text = _config.AudioFileType;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Hide();
        }
    }
}
