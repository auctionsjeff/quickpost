﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using WordPressPCL;

namespace QuickPost
{
    public partial class frmMain : Form
    {
        private WordPressClient client;
        private QuickPostConfig config;
        private string audioPath = "";
        private bool soxWorking = false;
        private SoxSharp.Sox sox;

        public frmMain()
        {
            InitializeComponent();
        }

        public void RefreshConfig()
        {
            string configPath = Application.StartupPath + "\\config.txt";
            if (!System.IO.File.Exists(configPath))
            {
                // make the default
                config = new QuickPostConfig
                {
                    AudioFileType = "mp3",
                    Category = "Test",
                    DefaultPostBody = "Post Body",
                    DefaultPostTitle = "Post Title",
                    Password = "secret",
                    Username = "admin",
                    CustomSoxArgs = "",
                    WebsiteUrl = "https://stpaulvancouver.com/",
                    AudioChannelCount = 1,
                    AudioSampleRateHz = 16000,
                    AudioVolumeModifier = 0.8M
                };
                SaveSettings();
                return;
            }
            using (FileStream fs = new FileStream(configPath, FileMode.Open))
            {
                XmlSerializer xs = new XmlSerializer(typeof(QuickPostConfig));
                config = (QuickPostConfig)xs.Deserialize(fs);
            }

            txtTitle.Text = config.DefaultPostTitle;
            txtBody.Text = config.DefaultPostBody;
        }

        public async Task<bool> TryLogin()
        {
            //return true;
            try
            {
                // Initialize
                client = new WordPressClient(config.WebsiteUrl.Trim('/') + "/wp-json/");
                // JWT authentication
                client.AuthMethod = WordPressPCL.Models.AuthMethod.JWT;
                await client.RequestJWToken(config.Username, config.Password);

                // check if authentication has been successful
                var isValidToken = await client.IsValidJWToken();

                return await Task.FromResult(isValidToken);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            RefreshConfig();

            // check if sox is installed
            if (!FindSox())
            {
                var msgbox = MessageBox.Show("Sox is not installed. Please install sox. View download page?", "Install Sox?", MessageBoxButtons.YesNo);
                if (msgbox == DialogResult.Yes)
                {
                    Process.Start("https://sourceforge.net/projects/sox/files/latest/download");
                }
            }

            bool successful = RunTryLoginSync();
            while (!successful)
            {

                // show login form
                var login = new Login(config);
                var result = login.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // grab any new settings
                    config.Username = login.txtUser.Text;
                    config.Password = login.txtPass.Text;
                    config.WebsiteUrl = login.txtUrl.Text;

                    successful = RunTryLoginSync();

                    // if they worked, persist the new settings
                    if (successful) SaveSettings();
                }
                else
                {
                    successful = true;
                    Application.Exit();
                }
            }

            // we are now logged in, and WP client is setup
        }

        private bool RunTryLoginSync()
        {
            var task = Task.Run(async () => await TryLogin());
            if (task.IsFaulted && task.Exception != null)
            {
                throw task.Exception;
            }

            return task.Result;
        }

        private void SaveSettings()
        {
            using (FileStream fs = new FileStream(Application.StartupPath + "\\config.txt", FileMode.OpenOrCreate))
            {
                XmlSerializer xs = new XmlSerializer(typeof(QuickPostConfig));
                xs.Serialize(fs, config);
            }
        }

        private bool FindSox()
        {
            if (!string.IsNullOrEmpty(config.SoxPath) && File.Exists(config.SoxPath))
            {
                return true;
            }

            // look for sox in prog files
            List<string> folders = Directory.EnumerateDirectories(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "sox-*").ToList();
            if (folders.Count > 0)
            {
                // found sox!
                config.SoxPath = Directory.EnumerateFiles(folders[0], "sox.exe").First();
            }

            return !string.IsNullOrEmpty(config.SoxPath) && File.Exists(config.SoxPath);
        }

        private void Convert()
        {
            soxWorking = true;
            pbConvert.Show();
            if (sox == null)
            {
                sox = new SoxSharp.Sox(config.SoxPath);
                sox.OnProgress += Sox_OnProgress;
            }

            if (string.IsNullOrEmpty(config.AudioFileType))
            {
                config.AudioFileType = "wav";
            }

            if (config.AudioChannelCount <= 0 || config.AudioChannelCount > 2)
            {
                config.AudioChannelCount = 2;
            }

            if (config.AudioSampleRateHz <= 0 || config.AudioSampleRateHz > 96000)
            {
                config.AudioSampleRateHz = 96000;
            }

            string newPath = System.IO.Path.GetFileNameWithoutExtension(audioPath) + "_converted." + config.AudioFileType.ToString();
            // add folder
            newPath = Directory.GetParent(audioPath) + "\\" + newPath;
            sox.CustomArgs = config.CustomSoxArgs;
            sox.Effects.Add(new SoxSharp.Effects.LoudnessEffect((double)config.AudioVolumeModifier));
            switch (config.AudioFileType.ToLower())
            {
                case "mp3":
                    sox.Output.Type = SoxSharp.FileType.MP3;
                    break;
                default:
                    sox.Output.Type = SoxSharp.FileType.WAV;
                    break;
            }
            sox.Output.SampleRate = (uint)config.AudioSampleRateHz;
            sox.Output.Channels = (ushort)config.AudioChannelCount;
            sox.Process(audioPath, newPath);

            audioPath = newPath;
        }

        delegate void ProgressDelegate(object sender, SoxSharp.ProgressEventArgs e);

        private void Sox_OnProgress(object sender, SoxSharp.ProgressEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new ProgressDelegate(Sox_OnProgress), new[] { sender, e });
            }
            else
            {
                pbConvert.Value = e.Progress;
                if (e.Progress == 100)
                {
                    pbConvert.Hide();
                    soxWorking = false;
                    btnPost.Enabled = true;
                }
            }

        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings frmSettings = new Settings(config);

            if (frmSettings.ShowDialog() == DialogResult.OK)
            {
                config.WebsiteUrl = frmSettings.txtUrl.Text;
                config.Username = frmSettings.txtUser.Text;
                config.Password = frmSettings.txtPass.Text;
                config.Category = frmSettings.txtCategory.Text;

                config.DefaultPostTitle = frmSettings.txtDefaultTitle.Text;
                config.DefaultPostBody = frmSettings.txtDefaultBody.Text;

                config.AudioChannelCount = int.Parse(frmSettings.txtChannels.Text);
                config.AudioSampleRateHz = int.Parse(frmSettings.txtSampleRate.Text);   
                config.CustomSoxArgs = frmSettings.txtCustomArgs.Text;
                config.AudioVolumeModifier = decimal.Parse(frmSettings.txtVolumeModifier.Text);
                config.AudioFileType = frmSettings.txtFileType.Text;

                SaveSettings();
            }
        }

        private void lblDrop_DragEnter_1(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
            //System.Diagnostics.Debug.WriteLine(e.Data.GetFormats());
        }

        private void lblDrop_Click_1(object sender, EventArgs e)
        {
            // show browser
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            ofd.InitialDirectory = desktop;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                audioPath = ofd.FileName;
                lblDrop.Text = "Selected " + Path.GetFileName(audioPath);
            }

            if (audioPath.Length < 1 || !File.Exists(audioPath)) return;
            // start conversion
            Convert();
        }

        private void lblDrop_DragDrop_1(object sender, DragEventArgs e)
        {
            var dropped = ((string[])e.Data.GetData(DataFormats.FileDrop));
            var files = dropped.ToList();

            if (!files.Any())
                return;

            audioPath = files.First();

            if (audioPath.Length < 1 || !File.Exists(audioPath)) return;
            // start conversion
            Convert();
        }

        private void btnPost_Click(object sender, EventArgs e)
        {

            // check title, body
            if (txtBody.Text.Length == 0 || txtTitle.Text.Length == 0)
            {
                MessageBox.Show("Please enter a title and body", "Missing Text", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // check audio
            if (string.IsNullOrEmpty(audioPath))
            {
                if (MessageBox.Show("Are you sure you want to post without any audio?", "No Audio", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }

            var cats = client.Categories.GetAll().Result;
            var matches = cats.Where(c => c.Name.ToLower() == config.Category.ToLower());
            int catId;
            if (matches.Count() == 0)
            {
                catId = cats.First().Id;
            }
            else
            {
                catId = matches.First() == null ? cats.First().Id : matches.First().Id;
            }

            if (catId == 0)
            {
                catId = cats.First().Id;
            }
            WordPressPCL.Models.Post post = new WordPressPCL.Models.Post
            {
                Categories = new[] { catId },
                Title = new WordPressPCL.Models.Title(txtTitle.Text)
            };





            var media = client.Media.Create(audioPath, Path.GetFileName(audioPath)).Result;
            post.FeaturedMedia = media.Id;

            string contentTemplate = "<!-- wp:audio {\"id\":" + media.Id + "} -->\r\n" +
                "<figure class=\"wp-block-audio\"><audio controls src=\"" + media.SourceUrl + "\"></audio><figcaption>" + txtTitle.Text + "</figcaption></figure>\r\n" +
                "<!-- /wp:audio -->\r\n" +
                "            \r\n" +
                "<!-- wp:paragraph -->\r\n" +
                "<p>" + txtBody.Text + "</p>\r\n" +
                "<!-- /wp:paragraph -->";

            post.Content = new WordPressPCL.Models.Content(contentTemplate);
            var result = client.Posts.Create(post).Result;

            if (string.IsNullOrEmpty(result.Link))
            {
                MessageBox.Show("The post was not created.");
            }
            else
            {
                Process.Start(result.Link);
            }
        }
    }
}
