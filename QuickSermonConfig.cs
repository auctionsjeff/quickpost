﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSermon
{
    public class QuickSermonConfig
    {
        public string WebsiteUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Category { get; set; }
        public string DefaultPostTitle { get; set; }
        public string DefaultPostBody { get; set; }
        public string AudioFileNameFormat { get; set; }
        public string SoxCommand { get; set; }
    }
}
